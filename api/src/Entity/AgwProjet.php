<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\AgwProjetRepository")
 */
class AgwProjet
{

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="agwProjets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $agent;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Projet", inversedBy="agwProjets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $projet;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $validation;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $dateproposition;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $dateattribution;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $dateValidation;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;


    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getValidation(): ?bool
    {
        return $this->validation;
    }

    public function setValidation(?bool $validation): self
    {
        $this->validation = $validation;

        return $this;
    }

    public function getDateproposition(): ?\DateTimeInterface
    {
        return $this->dateproposition;
    }

    public function setDateproposition(?\DateTimeInterface $dateproposition): self
    {
        $this->dateproposition = $dateproposition;

        return $this;
    }

    public function getDateattribution(): ?\DateTimeInterface
    {
        return $this->dateattribution;
    }

    public function setDateattribution(?\DateTimeInterface $dateattribution): self
    {
        $this->dateattribution = $dateattribution;

        return $this;
    }

    public function getDateValidation(): ?\DateTimeInterface
    {
        return $this->dateValidation;
    }

    public function setDateValidation(?\DateTimeInterface $dateValidation): self
    {
        $this->dateValidation = $dateValidation;

        return $this;
    }

    public function getNote(): ?float
    {
        return $this->note;
    }

    public function setNote(?float $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAgent(): ?user
    {
        return $this->agent;
    }

    public function setAgent(?user $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getProjet(): ?projet
    {
        return $this->projet;
    }

    public function setProjet(?projet $projet): self
    {
        $this->projet = $projet;

        return $this;
    }
}
