<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $datemeeting;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adressemeeting;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $codepostalmeeting;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgwFormation", mappedBy="formation")
     */
    private $agwFormations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chapitre", mappedBy="formation")
     */
    private $chapitres;

    public function __construct()
    {
        $this->agwFormations = new ArrayCollection();
        $this->chapitres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDatemeeting(): ?\DateTimeInterface
    {
        return $this->datemeeting;
    }

    public function setDatemeeting(?\DateTimeInterface $datemeeting): self
    {
        $this->datemeeting = $datemeeting;

        return $this;
    }

    public function getAdressemeeting(): ?string
    {
        return $this->adressemeeting;
    }

    public function setAdressemeeting(?string $adressemeeting): self
    {
        $this->adressemeeting = $adressemeeting;

        return $this;
    }

    public function getCodepostalmeeting(): ?string
    {
        return $this->codepostalmeeting;
    }

    public function setCodepostalmeeting(?string $codepostalmeeting): self
    {
        $this->codepostalmeeting = $codepostalmeeting;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|AgwFormation[]
     */
    public function getAgwFormations(): Collection
    {
        return $this->agwFormations;
    }

    public function addAgwFormation(AgwFormation $agwFormation): self
    {
        if (!$this->agwFormations->contains($agwFormation)) {
            $this->agwFormations[] = $agwFormation;
            $agwFormation->setFormation($this);
        }

        return $this;
    }

    public function removeAgwFormation(AgwFormation $agwFormation): self
    {
        if ($this->agwFormations->contains($agwFormation)) {
            $this->agwFormations->removeElement($agwFormation);
            // set the owning side to null (unless already changed)
            if ($agwFormation->getFormation() === $this) {
                $agwFormation->setFormation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Chapitre[]
     */
    public function getChapitres(): Collection
    {
        return $this->chapitres;
    }

    public function addChapitre(Chapitre $chapitre): self
    {
        if (!$this->chapitres->contains($chapitre)) {
            $this->chapitres[] = $chapitre;
            $chapitre->setFormation($this);
        }

        return $this;
    }

    public function removeChapitre(Chapitre $chapitre): self
    {
        if ($this->chapitres->contains($chapitre)) {
            $this->chapitres->removeElement($chapitre);
            // set the owning side to null (unless already changed)
            if ($chapitre->getFormation() === $this) {
                $chapitre->setFormation(null);
            }
        }

        return $this;
    }
}
