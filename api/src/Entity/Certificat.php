<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CertificatRepository")
 */
class Certificat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgwCertificat", mappedBy="certificat")
     */
    private $agwCertificats;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeCertificat", inversedBy="certificats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    public function __construct()
    {
        $this->agwCertificats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * @return Collection|AgwCertificat[]
     */
    public function getAgwCertificats(): Collection
    {
        return $this->agwCertificats;
    }

    public function addAgwCertificat(AgwCertificat $agwCertificat): self
    {
        if (!$this->agwCertificats->contains($agwCertificat)) {
            $this->agwCertificats[] = $agwCertificat;
            $agwCertificat->setCertificat($this);
        }

        return $this;
    }

    public function removeAgwCertificat(AgwCertificat $agwCertificat): self
    {
        if ($this->agwCertificats->contains($agwCertificat)) {
            $this->agwCertificats->removeElement($agwCertificat);
            // set the owning side to null (unless already changed)
            if ($agwCertificat->getCertificat() === $this) {
                $agwCertificat->setCertificat(null);
            }
        }

        return $this;
    }

    public function getType(): ?TypeCertificat
    {
        return $this->type;
    }

    public function setType(?TypeCertificat $type): self
    {
        $this->type = $type;

        return $this;
    }
}
