<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user_account")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $codepostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datenaissance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nationalit�e;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enNom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $enDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enSecteuractivite;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $enNbemployee;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enTva;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enCodeape;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activation;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $points;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disponible;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Facture", mappedBy="userP", orphanRemoval=true)
     */
    private $factures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Justificatif", mappedBy="userP", orphanRemoval=true)
     */
    private $justificatifs;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="agentsEntreprise")
     */
    private $lienEntreprise;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="lienEntreprise")
     */
    private $agentsEntreprise;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Projet", mappedBy="chefProjet")
     */
    private $chefProjets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Projet", mappedBy="client")
     */
    private $clientProjets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgwProjet", mappedBy="agent")
     */
    private $agwProjets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgwCertificat", mappedBy="agent")
     */
    private $certificats;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Grade", mappedBy="userP")
     */
    private $grades;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgwFormation", mappedBy="agent")
     */
    private $formation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgwTeste", mappedBy="agent")
     */
    private $agwTestes;

    public function __construct()
    {
        $this->factures = new ArrayCollection();
        $this->justificatifs = new ArrayCollection();
        $this->agentsEntreprise = new ArrayCollection();
        $this->chefProjets = new ArrayCollection();
        $this->clientProjets = new ArrayCollection();
        $this->agwProjets = new ArrayCollection();
        $this->certificats = new ArrayCollection();
        $this->grades = new ArrayCollection();
        $this->formation = new ArrayCollection();
        $this->agwTestes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodepostal(): ?string
    {
        return $this->codepostal;
    }

    public function setCodepostal(?string $codepostal): self
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getDatenaissance(): ?\DateTimeInterface
    {
        return $this->datenaissance;
    }

    public function setDatenaissance(?\DateTimeInterface $datenaissance): self
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    public function getNationalit�e(): ?string
    {
        return $this->nationalit�e;
    }

    public function setNationalit�e(?string $nationalit�e): self
    {
        $this->nationalit�e = $nationalit�e;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getEnNom(): ?string
    {
        return $this->enNom;
    }

    public function setEnNom(?string $enNom): self
    {
        $this->enNom = $enNom;

        return $this;
    }

    public function getEnDescription(): ?string
    {
        return $this->enDescription;
    }

    public function setEnDescription(?string $enDescription): self
    {
        $this->enDescription = $enDescription;

        return $this;
    }

    public function getEnSecteuractivite(): ?string
    {
        return $this->enSecteuractivite;
    }

    public function setEnSecteuractivite(?string $enSecteuractivite): self
    {
        $this->enSecteuractivite = $enSecteuractivite;

        return $this;
    }

    public function getEnNbemployee(): ?int
    {
        return $this->enNbemployee;
    }

    public function setEnNbemployee(?int $enNbemployee): self
    {
        $this->enNbemployee = $enNbemployee;

        return $this;
    }

    public function getEnTva(): ?string
    {
        return $this->enTva;
    }

    public function setEnTva(?string $enTva): self
    {
        $this->enTva = $enTva;

        return $this;
    }

    public function getEnCodeape(): ?string
    {
        return $this->enCodeape;
    }

    public function setEnCodeape(?string $enCodeape): self
    {
        $this->enCodeape = $enCodeape;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getActivation(): ?bool
    {
        return $this->activation;
    }

    public function setActivation(?bool $activation): self
    {
        $this->activation = $activation;

        return $this;
    }

    public function getpoints(): ?float
    {
        return $this->points;
    }

    public function setpoints(?float $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getDisponible(): ?bool
    {
        return $this->disponible;
    }

    public function setDisponible(?bool $disponible): self
    {
        $this->disponible = $disponible;

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures(): Collection
    {
        return $this->factures;
    }

    public function addFacture(Facture $facture): self
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setUserP($this);
        }

        return $this;
    }

    public function removeFacture(Facture $facture): self
    {
        if ($this->factures->contains($facture)) {
            $this->factures->removeElement($facture);
            // set the owning side to null (unless already changed)
            if ($facture->getUserP() === $this) {
                $facture->setUserP(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Justificatif[]
     */
    public function getJustificatifs(): Collection
    {
        return $this->justificatifs;
    }

    public function addJustificatif(Justificatif $justificatif): self
    {
        if (!$this->justificatifs->contains($justificatif)) {
            $this->justificatifs[] = $justificatif;
            $justificatif->setUserP($this);
        }

        return $this;
    }

    public function removeJustificatif(Justificatif $justificatif): self
    {
        if ($this->justificatifs->contains($justificatif)) {
            $this->justificatifs->removeElement($justificatif);
            // set the owning side to null (unless already changed)
            if ($justificatif->getUserP() === $this) {
                $justificatif->setUserP(null);
            }
        }

        return $this;
    }

    public function getLienEntreprise(): ?self
    {
        return $this->lienEntreprise;
    }

    public function setLienEntreprise(?self $lienEntreprise): self
    {
        $this->lienEntreprise = $lienEntreprise;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getAgentsEntreprise(): Collection
    {
        return $this->agentsEntreprise;
    }

    public function addAgentsEntreprise(self $agentsEntreprise): self
    {
        if (!$this->agentsEntreprise->contains($agentsEntreprise)) {
            $this->agentsEntreprise[] = $agentsEntreprise;
            $agentsEntreprise->setLienEntreprise($this);
        }

        return $this;
    }

    public function removeAgentsEntreprise(self $agentsEntreprise): self
    {
        if ($this->agentsEntreprise->contains($agentsEntreprise)) {
            $this->agentsEntreprise->removeElement($agentsEntreprise);
            // set the owning side to null (unless already changed)
            if ($agentsEntreprise->getLienEntreprise() === $this) {
                $agentsEntreprise->setLienEntreprise(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getChefProjets(): Collection
    {
        return $this->chefProjets;
    }

    public function addChefProjet(Projet $chefProjet): self
    {
        if (!$this->chefProjets->contains($chefProjet)) {
            $this->chefProjets[] = $chefProjet;
            $chefProjet->setChefProjet($this);
        }

        return $this;
    }

    public function removeChefProjet(Projet $chefProjet): self
    {
        if ($this->chefProjets->contains($chefProjet)) {
            $this->chefProjets->removeElement($chefProjet);
            // set the owning side to null (unless already changed)
            if ($chefProjet->getChefProjet() === $this) {
                $chefProjet->setChefProjet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getClientProjets(): Collection
    {
        return $this->clientProjets;
    }

    public function addClientProjet(Projet $clientProjet): self
    {
        if (!$this->clientProjets->contains($clientProjet)) {
            $this->clientProjets[] = $clientProjet;
            $clientProjet->setClient($this);
        }

        return $this;
    }

    public function removeClientProjet(Projet $clientProjet): self
    {
        if ($this->clientProjets->contains($clientProjet)) {
            $this->clientProjets->removeElement($clientProjet);
            // set the owning side to null (unless already changed)
            if ($clientProjet->getClient() === $this) {
                $clientProjet->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgwProjet[]
     */
    public function getAgwProjets(): Collection
    {
        return $this->agwProjets;
    }

    public function addAgwProjet(AgwProjet $agwProjet): self
    {
        if (!$this->agwProjets->contains($agwProjet)) {
            $this->agwProjets[] = $agwProjet;
            $agwProjet->setAgent($this);
        }

        return $this;
    }

    public function removeAgwProjet(AgwProjet $agwProjet): self
    {
        if ($this->agwProjets->contains($agwProjet)) {
            $this->agwProjets->removeElement($agwProjet);
            // set the owning side to null (unless already changed)
            if ($agwProjet->getAgent() === $this) {
                $agwProjet->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgwCertificat[]
     */
    public function getCertificats(): Collection
    {
        return $this->certificats;
    }

    public function addCertificat(AgwCertificat $certificat): self
    {
        if (!$this->certificats->contains($certificat)) {
            $this->certificats[] = $certificat;
            $certificat->setAgent($this);
        }

        return $this;
    }

    public function removeCertificat(AgwCertificat $certificat): self
    {
        if ($this->certificats->contains($certificat)) {
            $this->certificats->removeElement($certificat);
            // set the owning side to null (unless already changed)
            if ($certificat->getAgent() === $this) {
                $certificat->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Grade[]
     */
    public function getGrades(): Collection
    {
        return $this->grades;
    }

    public function addGrade(Grade $grade): self
    {
        if (!$this->grades->contains($grade)) {
            $this->grades[] = $grade;
            $grade->addUserP($this);
        }

        return $this;
    }

    public function removeGrade(Grade $grade): self
    {
        if ($this->grades->contains($grade)) {
            $this->grades->removeElement($grade);
            $grade->removeUserP($this);
        }

        return $this;
    }

    /**
     * @return Collection|AgwFormation[]
     */
    public function getFormation(): Collection
    {
        return $this->formation;
    }

    public function addFormation(AgwFormation $formation): self
    {
        if (!$this->formation->contains($formation)) {
            $this->formation[] = $formation;
            $formation->setAgent($this);
        }

        return $this;
    }

    public function removeFormation(AgwFormation $formation): self
    {
        if ($this->formation->contains($formation)) {
            $this->formation->removeElement($formation);
            // set the owning side to null (unless already changed)
            if ($formation->getAgent() === $this) {
                $formation->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgwTeste[]
     */
    public function getAgwTestes(): Collection
    {
        return $this->agwTestes;
    }

    public function addAgwTestis(AgwTeste $agwTestis): self
    {
        if (!$this->agwTestes->contains($agwTestis)) {
            $this->agwTestes[] = $agwTestis;
            $agwTestis->setAgent($this);
        }

        return $this;
    }

    public function removeAgwTestis(AgwTeste $agwTestis): self
    {
        if ($this->agwTestes->contains($agwTestis)) {
            $this->agwTestes->removeElement($agwTestis);
            // set the owning side to null (unless already changed)
            if ($agwTestis->getAgent() === $this) {
                $agwTestis->setAgent(null);
            }
        }

        return $this;
    }
}
