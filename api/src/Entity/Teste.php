<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="TesteRepository")
 */
class Teste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Titre;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Cours", inversedBy="teste", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $cours;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Question", mappedBy="teste")
     */
    private $questions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgwTeste", mappedBy="test")
     */
    private $agwTestes;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->agwTestes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setTeste($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            // set the owning side to null (unless already changed)
            if ($question->getTeste() === $this) {
                $question->setTeste(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgwTeste[]
     */
    public function getAgwTestes(): Collection
    {
        return $this->agwTestes;
    }

    public function addAgwTestis(AgwTeste $agwTestis): self
    {
        if (!$this->agwTestes->contains($agwTestis)) {
            $this->agwTestes[] = $agwTestis;
            $agwTestis->setTest($this);
        }

        return $this;
    }

    public function removeAgwTestis(AgwTeste $agwTestis): self
    {
        if ($this->agwTestes->contains($agwTestis)) {
            $this->agwTestes->removeElement($agwTestis);
            // set the owning side to null (unless already changed)
            if ($agwTestis->getTest() === $this) {
                $agwTestis->setTest(null);
            }
        }

        return $this;
    }
}
