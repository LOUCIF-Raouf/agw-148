<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ProjetRepository")
 */
class Projet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $objet;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $categorie;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $datedebut;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $datefin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $classe;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paiement;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="chefProjets")
     */
    private $chefProjet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="clientProjets")
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AgwProjet", mappedBy="projet")
     */
    private $agwProjets;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function __construct()
    {
        $this->agwProjets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObjet(): ?string
    {
        return $this->objet;
    }

    public function setObjet(string $objet): self
    {
        $this->objet = $objet;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    public function getClasse(): ?string
    {
        return $this->classe;
    }

    public function setClasse(string $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    public function getPaiement(): ?bool
    {
        return $this->paiement;
    }

    public function setPaiement(bool $paiement): self
    {
        $this->paiement = $paiement;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getChefProjet(): ?User
    {
        return $this->chefProjet;
    }

    public function setChefProjet(?User $chefProjet): self
    {
        $this->chefProjet = $chefProjet;

        return $this;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|AgwProjet[]
     */
    public function getAgwProjets(): Collection
    {
        return $this->agwProjets;
    }

    public function addAgwProjet(AgwProjet $agwProjet): self
    {
        if (!$this->agwProjets->contains($agwProjet)) {
            $this->agwProjets[] = $agwProjet;
            $agwProjet->setProjet($this);
        }

        return $this;
    }

    public function removeAgwProjet(AgwProjet $agwProjet): self
    {
        if ($this->agwProjets->contains($agwProjet)) {
            $this->agwProjets->removeElement($agwProjet);
            // set the owning side to null (unless already changed)
            if ($agwProjet->getProjet() === $this) {
                $agwProjet->setProjet(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
