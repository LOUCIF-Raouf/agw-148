<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200305001305 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE certificat_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chapitre_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE cours_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE etape_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE facture_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE formation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE grade_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE justificatif_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE projet_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE question_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE reponse_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE support_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE teste_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE type_certificat_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE agw_certificat (agent_id INT NOT NULL, certificat_id INT NOT NULL, create_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(agent_id, certificat_id))');
        $this->addSql('CREATE INDEX IDX_CCB36D063414710B ON agw_certificat (agent_id)');
        $this->addSql('CREATE INDEX IDX_CCB36D06FA55BACF ON agw_certificat (certificat_id)');
        $this->addSql('CREATE TABLE agw_formation (formation_id INT NOT NULL, agent_id INT NOT NULL, date_debut TIMESTAMP(0) WITH TIME ZONE NOT NULL, date_fin TIMESTAMP(0) WITH TIME ZONE NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(formation_id, agent_id))');
        $this->addSql('CREATE INDEX IDX_B9946F7A5200282E ON agw_formation (formation_id)');
        $this->addSql('CREATE INDEX IDX_B9946F7A3414710B ON agw_formation (agent_id)');
        $this->addSql('CREATE TABLE agw_projet (agent_id INT NOT NULL, projet_id INT NOT NULL, etat VARCHAR(255) NOT NULL, validation BOOLEAN DEFAULT NULL, dateproposition TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, dateattribution TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, date_validation TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, note DOUBLE PRECISION DEFAULT NULL, comment TEXT DEFAULT NULL, PRIMARY KEY(agent_id, projet_id))');
        $this->addSql('CREATE INDEX IDX_7772D7883414710B ON agw_projet (agent_id)');
        $this->addSql('CREATE INDEX IDX_7772D788C18272 ON agw_projet (projet_id)');
        $this->addSql('CREATE TABLE agw_teste (test_id INT NOT NULL, agent_id INT NOT NULL, note DOUBLE PRECISION NOT NULL, PRIMARY KEY(test_id, agent_id))');
        $this->addSql('CREATE INDEX IDX_83BEC27E1E5D0459 ON agw_teste (test_id)');
        $this->addSql('CREATE INDEX IDX_83BEC27E3414710B ON agw_teste (agent_id)');
        $this->addSql('CREATE TABLE certificat (id INT NOT NULL, type_id INT NOT NULL, titre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_27448F77C54C8C93 ON certificat (type_id)');
        $this->addSql('CREATE TABLE chapitre (id INT NOT NULL, formation_id INT NOT NULL, titre VARCHAR(255) NOT NULL, difficulte DOUBLE PRECISION DEFAULT NULL, numchapitre INT NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8C62B0255200282E ON chapitre (formation_id)');
        $this->addSql('CREATE TABLE cours (id INT NOT NULL, chapitre_id INT NOT NULL, titre VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, content TEXT DEFAULT NULL, numcours INT DEFAULT NULL, duree INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FDCA8C9C1FBEEF7B ON cours (chapitre_id)');
        $this->addSql('CREATE TABLE etape (id INT NOT NULL, numero INT NOT NULL, titre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE facture (id INT NOT NULL, user_p_id INT NOT NULL, label VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FE866410A9FA2F6B ON facture (user_p_id)');
        $this->addSql('CREATE TABLE formation (id INT NOT NULL, nom VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, datemeeting TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, adressemeeting VARCHAR(255) DEFAULT NULL, codepostalmeeting VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE grade (id INT NOT NULL, titre VARCHAR(255) NOT NULL, logo VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE grade_user (grade_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(grade_id, user_id))');
        $this->addSql('CREATE INDEX IDX_23F19B59FE19A1A8 ON grade_user (grade_id)');
        $this->addSql('CREATE INDEX IDX_23F19B59A76ED395 ON grade_user (user_id)');
        $this->addSql('CREATE TABLE justificatif (id INT NOT NULL, user_p_id INT NOT NULL, label VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_90D3C5DCA9FA2F6B ON justificatif (user_p_id)');
        $this->addSql('CREATE TABLE projet (id INT NOT NULL, chef_projet_id INT DEFAULT NULL, client_id INT DEFAULT NULL, objet VARCHAR(255) NOT NULL, categorie VARCHAR(255) NOT NULL, datedebut TIMESTAMP(0) WITH TIME ZONE NOT NULL, datefin TIMESTAMP(0) WITH TIME ZONE NOT NULL, classe VARCHAR(255) NOT NULL, paiement BOOLEAN NOT NULL, etat VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_50159CA9D3B0D67C ON projet (chef_projet_id)');
        $this->addSql('CREATE INDEX IDX_50159CA919EB6921 ON projet (client_id)');
        $this->addSql('CREATE TABLE question (id INT NOT NULL, teste_id INT NOT NULL, content TEXT NOT NULL, numero INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6F7494E80AA0132 ON question (teste_id)');
        $this->addSql('CREATE TABLE reponse (id INT NOT NULL, question_id INT NOT NULL, content TEXT NOT NULL, valide BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5FB6DEC71E27F6BF ON reponse (question_id)');
        $this->addSql('CREATE TABLE support (id INT NOT NULL, cours_id INT NOT NULL, titre VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, type VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, ordre INT NOT NULL, duree DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8004EBA57ECF78B0 ON support (cours_id)');
        $this->addSql('CREATE TABLE teste (id INT NOT NULL, cours_id INT NOT NULL, titre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E6B4490F7ECF78B0 ON teste (cours_id)');
        $this->addSql('CREATE TABLE type_certificat (id INT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_account (id INT NOT NULL, lien_entreprise_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, tel VARCHAR(255) DEFAULT NULL, gender VARCHAR(255) DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, codepostal VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, datenaissance DATE DEFAULT NULL, nationalit�e VARCHAR(255) DEFAULT NULL, siret VARCHAR(255) DEFAULT NULL, en_nom VARCHAR(255) DEFAULT NULL, en_description TEXT DEFAULT NULL, en_secteuractivite VARCHAR(255) DEFAULT NULL, en_nbemployee INT DEFAULT NULL, en_tva VARCHAR(255) DEFAULT NULL, en_codeape VARCHAR(255) DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, etat VARCHAR(255) DEFAULT NULL, activation BOOLEAN DEFAULT NULL, points DOUBLE PRECISION DEFAULT NULL, disponible BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_253B48AEE7927C74 ON user_account (email)');
        $this->addSql('CREATE INDEX IDX_253B48AEB86A36C0 ON user_account (lien_entreprise_id)');
        $this->addSql('ALTER TABLE agw_certificat ADD CONSTRAINT FK_CCB36D063414710B FOREIGN KEY (agent_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agw_certificat ADD CONSTRAINT FK_CCB36D06FA55BACF FOREIGN KEY (certificat_id) REFERENCES certificat (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agw_formation ADD CONSTRAINT FK_B9946F7A5200282E FOREIGN KEY (formation_id) REFERENCES formation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agw_formation ADD CONSTRAINT FK_B9946F7A3414710B FOREIGN KEY (agent_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agw_projet ADD CONSTRAINT FK_7772D7883414710B FOREIGN KEY (agent_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agw_projet ADD CONSTRAINT FK_7772D788C18272 FOREIGN KEY (projet_id) REFERENCES projet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agw_teste ADD CONSTRAINT FK_83BEC27E1E5D0459 FOREIGN KEY (test_id) REFERENCES teste (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agw_teste ADD CONSTRAINT FK_83BEC27E3414710B FOREIGN KEY (agent_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE certificat ADD CONSTRAINT FK_27448F77C54C8C93 FOREIGN KEY (type_id) REFERENCES type_certificat (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chapitre ADD CONSTRAINT FK_8C62B0255200282E FOREIGN KEY (formation_id) REFERENCES formation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cours ADD CONSTRAINT FK_FDCA8C9C1FBEEF7B FOREIGN KEY (chapitre_id) REFERENCES chapitre (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE866410A9FA2F6B FOREIGN KEY (user_p_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE grade_user ADD CONSTRAINT FK_23F19B59FE19A1A8 FOREIGN KEY (grade_id) REFERENCES grade (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE grade_user ADD CONSTRAINT FK_23F19B59A76ED395 FOREIGN KEY (user_id) REFERENCES user_account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE justificatif ADD CONSTRAINT FK_90D3C5DCA9FA2F6B FOREIGN KEY (user_p_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE projet ADD CONSTRAINT FK_50159CA9D3B0D67C FOREIGN KEY (chef_projet_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE projet ADD CONSTRAINT FK_50159CA919EB6921 FOREIGN KEY (client_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E80AA0132 FOREIGN KEY (teste_id) REFERENCES teste (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reponse ADD CONSTRAINT FK_5FB6DEC71E27F6BF FOREIGN KEY (question_id) REFERENCES question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE support ADD CONSTRAINT FK_8004EBA57ECF78B0 FOREIGN KEY (cours_id) REFERENCES cours (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE teste ADD CONSTRAINT FK_E6B4490F7ECF78B0 FOREIGN KEY (cours_id) REFERENCES cours (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_account ADD CONSTRAINT FK_253B48AEB86A36C0 FOREIGN KEY (lien_entreprise_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE agw_certificat DROP CONSTRAINT FK_CCB36D06FA55BACF');
        $this->addSql('ALTER TABLE cours DROP CONSTRAINT FK_FDCA8C9C1FBEEF7B');
        $this->addSql('ALTER TABLE support DROP CONSTRAINT FK_8004EBA57ECF78B0');
        $this->addSql('ALTER TABLE teste DROP CONSTRAINT FK_E6B4490F7ECF78B0');
        $this->addSql('ALTER TABLE agw_formation DROP CONSTRAINT FK_B9946F7A5200282E');
        $this->addSql('ALTER TABLE chapitre DROP CONSTRAINT FK_8C62B0255200282E');
        $this->addSql('ALTER TABLE grade_user DROP CONSTRAINT FK_23F19B59FE19A1A8');
        $this->addSql('ALTER TABLE agw_projet DROP CONSTRAINT FK_7772D788C18272');
        $this->addSql('ALTER TABLE reponse DROP CONSTRAINT FK_5FB6DEC71E27F6BF');
        $this->addSql('ALTER TABLE agw_teste DROP CONSTRAINT FK_83BEC27E1E5D0459');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494E80AA0132');
        $this->addSql('ALTER TABLE certificat DROP CONSTRAINT FK_27448F77C54C8C93');
        $this->addSql('ALTER TABLE agw_certificat DROP CONSTRAINT FK_CCB36D063414710B');
        $this->addSql('ALTER TABLE agw_formation DROP CONSTRAINT FK_B9946F7A3414710B');
        $this->addSql('ALTER TABLE agw_projet DROP CONSTRAINT FK_7772D7883414710B');
        $this->addSql('ALTER TABLE agw_teste DROP CONSTRAINT FK_83BEC27E3414710B');
        $this->addSql('ALTER TABLE facture DROP CONSTRAINT FK_FE866410A9FA2F6B');
        $this->addSql('ALTER TABLE grade_user DROP CONSTRAINT FK_23F19B59A76ED395');
        $this->addSql('ALTER TABLE justificatif DROP CONSTRAINT FK_90D3C5DCA9FA2F6B');
        $this->addSql('ALTER TABLE projet DROP CONSTRAINT FK_50159CA9D3B0D67C');
        $this->addSql('ALTER TABLE projet DROP CONSTRAINT FK_50159CA919EB6921');
        $this->addSql('ALTER TABLE user_account DROP CONSTRAINT FK_253B48AEB86A36C0');
        $this->addSql('DROP SEQUENCE certificat_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chapitre_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE cours_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE etape_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE facture_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE formation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE grade_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE justificatif_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE projet_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE question_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE reponse_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE support_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE teste_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE type_certificat_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_account_id_seq CASCADE');
        $this->addSql('DROP TABLE agw_certificat');
        $this->addSql('DROP TABLE agw_formation');
        $this->addSql('DROP TABLE agw_projet');
        $this->addSql('DROP TABLE agw_teste');
        $this->addSql('DROP TABLE certificat');
        $this->addSql('DROP TABLE chapitre');
        $this->addSql('DROP TABLE cours');
        $this->addSql('DROP TABLE etape');
        $this->addSql('DROP TABLE facture');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE grade');
        $this->addSql('DROP TABLE grade_user');
        $this->addSql('DROP TABLE justificatif');
        $this->addSql('DROP TABLE projet');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE reponse');
        $this->addSql('DROP TABLE support');
        $this->addSql('DROP TABLE teste');
        $this->addSql('DROP TABLE type_certificat');
        $this->addSql('DROP TABLE user_account');
    }
}
