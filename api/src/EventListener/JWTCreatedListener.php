<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;


class JWTCreatedListener
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        // if (!$user instanceof UserInterface) {
        //     return;
        // }


        $data['data'] = array(
            'last_name' => $user->getNom(),
            'first_name' => $user->getPrenom(),
            'id' => $user->getId(),
            'pic' => $user->getPhoto(),
            'tel' => $user->getTel(),
            'gender' => $user->getGender(),
            'adresse' => $user->getAdresse(),
            'cdp' => $user->getCodepostal(),
            'ville' => $user->getVille(),
            'birthday' => $user->getDatenaissance(),
            'nationalite' => $user->getNationalit�e(),
            'siret' => $user->getSiret(),
            'company' => $user->getEnNom(),
            'company_desc' =>$user->getEnDescription(),
            'secteur_activite' => $user->getEnSecteuractivite(),
            'nb_employee' => $user->getEnNbemployee(),
            'company_tva' => $user->getEnTva(),
            'code_ape' => $user->getEnCodeape(),
            'state_account' => $user->getEtat(),
            'activated_account' => $user->getActivation(),
            'points' => $user->getPoints(),
            'available' => $user->getDisponible(),
        );

        $event->setData($data);
    }
}
