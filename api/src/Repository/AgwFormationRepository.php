<?php

namespace App\Repository;

use App\Entity\AgwFormation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AgwFormation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgwFormation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgwFormation[]    findAll()
 * @method AgwFormation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgwFormationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgwFormation::class);
    }

    // /**
    //  * @return AgwFormation[] Returns an array of AgwFormation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgwFormation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
