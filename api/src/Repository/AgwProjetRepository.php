<?php

namespace App\Repository;

use App\Entity\AgwProjet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AgwProjet|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgwProjet|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgwProjet[]    findAll()
 * @method AgwProjet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgwProjetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgwProjet::class);
    }

    // /**
    //  * @return AgwProjet[] Returns an array of AgwProjet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgwProjet
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
