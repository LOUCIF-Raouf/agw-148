<?php

namespace App\Repository;

use App\Entity\AgwTeste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AgwTeste|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgwTeste|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgwTeste[]    findAll()
 * @method AgwTeste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgwTesteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgwTeste::class);
    }

    // /**
    //  * @return AgwTeste[] Returns an array of AgwTeste objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgwTeste
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
